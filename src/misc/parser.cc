#include "parser.hh"

#include <cstdio>
#include <iostream>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <tbb/tick_count.h>
#include <filters/filter.hh>

/* Generators */
#include <filters/mandelbrot.hh>
#include <filters/burningship.hh>
#include <filters/constant.hh>
/* Outputs */
#include <filters/tga.hh>
#include <filters/sdl.hh>
/* Effects */
#include <filters/binarize.hh>
#include <filters/colorize.hh>
#include <filters/unify.hh>
#include <filters/sobel.hh>
#include <filters/blur.hh>
/* Combinations */
#include <filters/xor.hh>
#include <filters/and.hh>
#include <filters/or.hh>

void Parser::fill_filters_map()
{
    /* Generators */
    filters_["mandelbrot"] =
        filters::Filter_ptr(new filters::Mandelbrot(opts_));
    filters_["burningship"] =
        filters::Filter_ptr(new filters::Burningship(opts_));
    filters_["constant"] =
        filters::Filter_ptr(new filters::Constant(opts_));

    /* Outputs */
    filters_["tga"] =
        filters::Filter_ptr(new filters::TGA(opts_));
    filters_["sdl"] =
        filters::Filter_ptr(new filters::SDL(opts_));

    /* Effects */
    filters_["binarize"] =
        filters::Filter_ptr(new filters::Binarize(opts_));
    filters_["colorize"] =
        filters::Filter_ptr(new filters::Colorize(opts_));
    filters_["unify"] =
        filters::Filter_ptr(new filters::Unify(opts_));
    filters_["sobel"] =
        filters::Filter_ptr(new filters::Sobel(opts_));
    filters_["blur"] =
        filters::Filter_ptr(new filters::Blur(opts_));

    /* Combinations */
    filters_["xor"] =
        filters::Filter_ptr(new filters::Xor(opts_));
    filters_["and"] =
        filters::Filter_ptr(new filters::And(opts_));
    filters_["or"] =
        filters::Filter_ptr(new filters::Or(opts_));
}

void Parser::run()
{
    uint32_t index = 0;

    /* First image cumputations */
    filters::Filter_ptr filter = get_filter(opts_.instr()[index]);
    if (filter->nb_arg() != 0)
    {
        std::cerr << "First filter should be a generator" << std::endl;
        exit(1);
    }
    Image_ptr img(new Image(opts_.width(), opts_.height()));

    std::vector<Image_ptr> img_vect;
    img_vect.push_back(img);

    /* Verbose timer */
    tbb::tick_count t_beg = tbb::tick_count::now();

    filter->run(img_vect);

    /* Verbose timer */
    timer_end(t_beg, index);

    stack_.push(filter->img());
    ++index;

    /* General case */
    std::vector<std::string> vect;
    while (!stack_.empty() && index < opts_.instr().size())
    {
        filter = get_filter(opts_.instr()[index]);

        img_vect.clear();

        /* for generators */
        if (filter->nb_arg() == 0)
        {
            img.reset(new Image(opts_.width(), opts_.height()));
            img_vect.push_back(img);
        }

        for (uint32_t i = 0; i < filter->nb_arg(); ++i)
        {
            img_vect.push_back(stack_.top());
            stack_.pop();
        }

        /* Verbose timer */
        t_beg = tbb::tick_count::now();

        filter->run(img_vect);

        /* Verbose timer */
        timer_end(t_beg, index);

        stack_.push(filter->img());
        ++index;
    }
}

filters::Filter_ptr Parser::get_filter(const std::string& instr)
{
    VectStr_ptr args (new std::vector<std::string>());
    filters::Filter_ptr filter;

    try
    {
        boost::split(*args, instr, boost::is_any_of(","));
        filter = filters_.at((*args)[0]);
    }
    catch (std::exception& e)
    {
        std::cerr << "Unknown filter: " << (*args)[0] << std::endl;
        exit(1);
    }

    filter->init(args);
    return filter;
}

void Parser::timer_end(tbb::tick_count& t_beg, uint32_t index)
{
    tbb::tick_count t_end = tbb::tick_count::now();

    if (opts_.verbose())
    {
        std::cout << opts_.instr()[index] << ": "
            << (t_end - t_beg).seconds() << "s" << std::endl;
    }
}
