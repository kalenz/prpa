#ifndef IMAGE_HH
# define IMAGE_HH

# include <cstdint>
# include <cstring>
# include <memory>

class Image
{
public:
    Image(const Image& img);
    Image(uint16_t width, uint16_t height);
    virtual ~Image();

    void operator=(const Image& img);

    bool set(uint16_t w, uint16_t h, uint32_t value);
    uint32_t get(uint16_t w, uint16_t h) const;

    uint16_t width() const { return width_; }
    uint16_t height() const { return height_; }
    uint32_t size() const { return size_; }
    uint32_t max_iter() const { return max_iter_; }
    const uint32_t* data() const { return data_; }

    void max_iter_set(uint32_t max_iter) { max_iter_ = max_iter; }

protected:
    uint16_t width_;
    uint16_t height_;
    uint32_t size_;
    uint32_t max_iter_;
    uint32_t* data_;
};

typedef std::shared_ptr<Image> Image_ptr;

#endif // IMAGE_HH
