#ifndef COLOR_HH
# define COLOR_HH

# include <cstdint>

struct rgba
{
    uint8_t r;
    uint8_t g;
    uint8_t b;
    uint8_t a;
};

inline
uint32_t rgba(uint8_t r, uint8_t g, uint8_t b, uint8_t a)
{
    return (r << 24) + (g << 16) + (b << 8) + a;
}

#endif // COLOR_HH
