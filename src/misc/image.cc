#include "image.hh"

#include <cstring>

#define SQ(w, h) ((h) * width_ + (w))

Image::Image(const Image& img)
    : width_(img.width_),
      height_(img.height_),
      size_(img.size_),
      max_iter_(img.max_iter_)
{
    data_ = new uint32_t[size_];
    memcpy(data_, img.data_, size_ * sizeof (*data_));
}

void Image::operator=(const Image& img)
{
    width_ = img.width_;
    height_ = img.height_;
    size_ = width_ * height_;
    max_iter_ = img.max_iter_;

    delete[] data_;
    data_ = new uint32_t[size_];
    memcpy(data_, img.data_, size_ * sizeof (*data_));
}

Image::Image(uint16_t width, uint16_t height)
    : width_(width),
      height_(height),
      size_(width * height)
{
    data_ = new uint32_t[size_];
}

bool Image::set(uint16_t w, uint16_t h, uint32_t value)
{
    if (w > width_ || h > height_)
        return false;

    data_[SQ(w, h)] = value;
    return true;
}

uint32_t Image::get(uint16_t w, uint16_t h) const
{
    if (w > width_ || h > height_)
        return -1;

    return data_[SQ(w, h)];
}

Image::~Image()
{
    delete[] data_;
}
