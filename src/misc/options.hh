#ifndef OPTIONS_HH
# define OPTIONS_HH

# include <cstdint>
# include <string>
# include <vector>

# include <misc/types.hh>

class Options
{
    DISALLOW_COPY_AND_ASSIGN(Options)

public:
    Options() {}

    void process_cli(int argc, char** argv);

    bool verbose() const { return verbose_; }
    bool timer() const { return timer_; }
    uint32_t nb_worker() const { return nb_worker_; }
    uint16_t height() const { return height_; }
    uint16_t width() const { return width_; }
    const std::vector<std::string>& instr() const { return instr_; }

private:
    /* General */
    bool verbose_;
    bool timer_;
    std::vector<std::string> instr_;

    /* TBB */
    uint32_t nb_worker_;

    /* Window */
    uint16_t height_;
    uint16_t width_;
};

#endif // OPTIONS_HH
