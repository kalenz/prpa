#ifndef PARSER_HH
# define PARSER_HH

# include <vector>
# include <string>
# include <stack>
# include <unordered_map>
# include <utility>
# include <tbb/tick_count.h>

# include <misc/image.hh>
# include <misc/options.hh>
# include <filters/filter.hh>

class Parser
{
    DISALLOW_COPY_AND_ASSIGN(Parser)

public:
    Parser(const Options& opts);
    void run();

protected:
    void fill_filters_map();
    filters::Filter_ptr get_filter(const std::string& instr);

    void timer_end(tbb::tick_count& t_beg, uint32_t index);

private:
    const Options& opts_;
    std::stack<Image_ptr> stack_;
    std::unordered_map<std::string, filters::Filter_ptr> filters_;
};

inline
Parser::Parser(const Options& opts)
    : opts_(opts)
{
    fill_filters_map();
}

#endif // PARSER_HH
