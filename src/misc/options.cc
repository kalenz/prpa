#include "options.hh"

#include <iostream>
#include <boost/program_options.hpp>

void Options::process_cli(int argc, char** argv)
{
    namespace po = boost::program_options;

    po::options_description generic("PRPA");
    generic.add_options()
        /* General */
        ("help", "Display this help")
        ("verbose,v", "Verbose mode")
        ("timer,t", "Show rendering time")
        /* TBB */
        ("nb-worker,n", po::value<uint32_t>(&nb_worker_)->default_value(2),
            "Number of TBB worker")
        /* Window */
        ("width,w", po::value<uint16_t>(&width_)->default_value(800),
            "Width of the SDL display")
        ("height,h", po::value<uint16_t>(&height_)->default_value(600),
            "Height of the SDL display")
        ("input", po::value<std::vector<std::string>>(&instr_), "Input commands")
    ;

    po::positional_options_description pos;
    pos.add("input", -1);

    po::variables_map vm;
    try
    {
        po::store(po::command_line_parser(argc, argv)
                .options(generic).positional(pos).run(), vm);
    }
    catch (std::exception& e)
    {
        std::cerr << "Error: " << e.what() << std::endl;
        exit(1);
    }
    po::notify(vm);

    /* General */
    if (vm.count("help"))
    {
        std::cout << generic << std::endl;
        exit(0);
    }

    verbose_ = vm.count("verbose") ? true : false;
    timer_ = vm.count("timer") ? true : false;
}
