#ifndef TYPES_HH
# define TYPES_HH

# include <memory>
# include <vector>
# include <string>

typedef std::shared_ptr<std::vector<std::string>> VectStr_ptr;

# define DISALLOW_COPY_AND_ASSIGN(TypeName)     \
    TypeName(const TypeName&);                  \
    void operator=(const TypeName&);

#endif // TYPES_HH
