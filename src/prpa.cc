#include <iostream>
#include <misc/options.hh>
#include <misc/parser.hh>
#include "tbb/task_scheduler_init.h"
#include <tbb/tick_count.h>

int main(int argc, char** argv)
{
    Options opts;

    opts.process_cli(argc, argv);
    Parser p(opts);

    tbb::task_scheduler_init init(opts.nb_worker());

    tbb::tick_count t_beg = tbb::tick_count::now();
    p.run();
    tbb::tick_count t_end = tbb::tick_count::now();

    if (opts.timer())
        std::cout << "Total time: "
            << (t_end - t_beg).seconds() << "s" << std::endl;

    return 0;
}
