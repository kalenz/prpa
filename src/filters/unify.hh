#ifndef FILTERS_UNIFY_HH
# define FILTERS_UNIFY_HH

# include <cstdint>
# include <vector>
# include <string>
# include <memory>

# include <misc/types.hh>
# include <misc/image.hh>
# include <misc/color.hh>
# include <misc/options.hh>

# include "filter.hh"

namespace filters
{

class Unify : public Filter
{
    DISALLOW_COPY_AND_ASSIGN(Unify)

public:
    Unify(const Options& opts);
    virtual void apply(std::vector<Image_ptr>& imgs,
            uint16_t w, uint16_t h);
};

inline
Unify::Unify(const Options& opts)
    : Filter(opts)
{
    nb_arg_ = 1;
}

} // namespace filters

#endif // FILTERS_UNIFY_HH
