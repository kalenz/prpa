#include "tga.hh"

#include <vector>
#include <cstdint>

#include <misc/image.hh>

#include <display/tga.hh>

using namespace filters;

void TGA::init(VectStr_ptr argv)
{
    if (argv->size() <= 1)
    {
        std::cerr << "Not enough arguments for " << (*argv)[0] << std::endl;
        exit(1);
    }

    argv_ = argv;
    filename_ = (*argv)[1];
}

void TGA::run(std::vector<Image_ptr>& imgs)
{
    display::TGA disp;
    disp.init(imgs[0], filename_);
    disp.display();
    disp.quit();

    img_ = imgs[0];
}

void TGA::apply(std::vector<Image_ptr>& imgs, uint16_t w, uint16_t h)
{
    (void) imgs;
    (void) w;
    (void) h;
}
