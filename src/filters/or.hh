#ifndef FILTERS_OR_HH
# define FILTERS_OR_HH

# include <cstdint>
# include <vector>

# include <misc/types.hh>
# include <misc/image.hh>
# include <misc/options.hh>

# include "filter.hh"

namespace filters
{

class Or : public Filter
{
    DISALLOW_COPY_AND_ASSIGN(Or)

public:
    Or(const Options& opts);
    virtual void apply(std::vector<Image_ptr>& imgs,
            uint16_t w, uint16_t h);
};

inline
Or::Or(const Options& opts)
    : Filter(opts)
{
    nb_arg_ = 2;
}

} // namespace filters

#endif // FILTERS_OR_HH
