#include "constant.hh"

#include <cstdint>
#include <iostream>
#include <vector>
#include <string>
#include <misc/color.hh>

using namespace filters;

Constant::Constant(const Options& opts)
    : Set(opts)
{
    nb_arg_ = 0;
}

void Constant::init(VectStr_ptr argv)
{
    if (argv->size() <= 4)
    {
        std::cerr << "Not enough arguments for " << (*argv)[0] << std::endl;
        exit(1);
    }

    rgba_.r = atoi((*argv)[1].c_str());
    rgba_.g = atoi((*argv)[2].c_str());
    rgba_.b = atoi((*argv)[3].c_str());
    rgba_.a = atoi((*argv)[4].c_str());
}

void Constant::apply(std::vector<Image_ptr>& imgs, uint16_t w, uint16_t h)
{
    imgs[0]->set(w, h, rgba(rgba_.r, rgba_.g, rgba_.b, rgba_.a));
}
