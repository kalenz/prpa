#ifndef FILTERS_FILTER_HH
# define FILTERS_FILTER_HH

# include <cstdint>
# include <vector>
# include <string>
# include <memory>

# include <misc/types.hh>
# include <misc/image.hh>
# include <misc/options.hh>

namespace filters
{

class Filter
{
public:
    Filter(const Options& opts);
    virtual ~Filter();
    virtual void init(VectStr_ptr argv);
    virtual void run(std::vector<Image_ptr>& imgs);
    virtual void apply(std::vector<Image_ptr>& imgs,
            uint16_t w, uint16_t h) = 0;

    virtual uint32_t nb_arg() const { return nb_arg_; }
    virtual Image_ptr img() const { return img_; }

protected:
    Image_ptr img_;
    VectStr_ptr argv_;
    const Options& opts_;
    uint32_t nb_arg_;
};

typedef std::shared_ptr<Filter> Filter_ptr;

inline
Filter::Filter(const Options& opts)
    : opts_(opts),
      nb_arg_(1)
{
}

inline
Filter::~Filter()
{
}

inline
void Filter::init(VectStr_ptr argv)
{
    argv_ = argv;
}

} // namespace filters

#endif // FILTERS_FILTER_HH
