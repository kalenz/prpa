#include "unify.hh"

#include <iostream>
#include <misc/color.hh>

using namespace filters;

void Unify::apply(std::vector<Image_ptr>& imgs, uint16_t w, uint16_t h)
{
    uint32_t new_px = ((double)imgs[0]->get(w, h) /
            (double)imgs[0]->max_iter()) * 256;

    imgs[0]->set(w, h, rgba(new_px, new_px, new_px, -1));
}
