#ifndef FILTER_BURNINGSHIP_HH
# define FILTER_BURNINGSHIP_HH

# include <vector>
# include <misc/types.hh>
# include "set.hh"

namespace filters
{

class Burningship : public Set
{
    DISALLOW_COPY_AND_ASSIGN(Burningship)

public:
    Burningship(const Options& opts);
    virtual void apply(std::vector<Image_ptr>& imgs, uint16_t w, uint16_t h);
};

inline
Burningship::Burningship(const Options& opts)
    : Set(opts)
{
    nb_arg_ = 0;
}

} // namespace filters

#endif // FILTER_BURNINGSHIP_HH
