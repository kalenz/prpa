#include "set.hh"

#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <tbb/parallel_for.h>
#include <tbb/blocked_range2d.h>
#include <misc/types.hh>
#include <misc/image.hh>

using namespace filters;

Set::Set(const Options& opts)
    : Filter(opts)
{
    nb_arg_ = 0;
}

void Set::init(VectStr_ptr argv)
{
    if (argv->size() <= 7)
    {
        std::cerr << "Not enough arguments for " << (*argv)[0] << std::endl;
        exit(1);
    }

    argv_ = argv;
    sopts_.max_iter = atoi((*argv)[1].c_str());
    sopts_.center_x = strtod((*argv)[2].c_str(), NULL);
    sopts_.center_y = strtod((*argv)[3].c_str(), NULL);
    sopts_.min_re = strtod((*argv)[4].c_str(), NULL);
    sopts_.max_re = strtod((*argv)[5].c_str(), NULL);
    sopts_.min_im = strtod((*argv)[6].c_str(), NULL);
    sopts_.max_im = strtod((*argv)[7].c_str(), NULL);
}

void Set::run(std::vector<Image_ptr>& imgs)
{
    /* Ugly hack */
    imgs[0]->max_iter_set(sopts_.max_iter);

    parallel_for(
        tbb::blocked_range2d<size_t>(0, opts_.width(), 0, opts_.height()),
        [&](const tbb::blocked_range2d<size_t>& r)
        {
            for (size_t w = r.rows().begin(); w != r.rows().end(); ++w)
                for (size_t h = r.cols().begin(); h != r.cols().end(); ++h)
                    apply(imgs, w, h);
        }
    );

    img_.reset(new Image(*imgs[0]));
}
