#include "sobel.hh"

#include <cmath>
#include <tbb/parallel_for.h>
#include <tbb/blocked_range2d.h>

#include <misc/color.hh>

using namespace filters;

void Sobel::run(std::vector<Image_ptr>& imgs)
{
    Image_ptr new_img(new Image(opts_.width(), opts_.height()));
    imgs.push_back(new_img);

    parallel_for(
        tbb::blocked_range2d<size_t>(0, opts_.width(), 0, opts_.height()),
        [&](const tbb::blocked_range2d<size_t>& r)
        {
            for (size_t w = r.rows().begin(); w != r.rows().end(); ++w)
                for (size_t h = r.cols().begin(); h != r.cols().end(); ++h)
                    apply(imgs, w, h);
        }
    );

    img_ = imgs[1];
}

void Sobel::apply(std::vector<Image_ptr>& imgs, uint16_t w, uint16_t h)
{
    int gx[3][3];
    int gy[3][3];

    gx[0][0] = -1; gx[0][1] = 0; gx[0][2] = 1;
    gx[1][0] = -2; gx[1][1] = 0; gx[1][2] = 2;
    gx[2][0] = -1; gx[2][1] = 0; gx[2][2] = 1;

    gy[0][0] =  1; gy[0][1] =  2; gy[0][2] =  1;
    gy[1][0] =  0; gy[1][1] =  0; gy[1][2] =  0;
    gy[2][0] = -1; gy[2][1] = -2; gy[2][2] = -1;

    int32_t sum_x = 0;
    int32_t sum_y = 0;

    int32_t sum;

    if (w <= 0 || w >= opts_.width() - 1 ||
            h <= 0 || h >= opts_.height() - 1)
        sum = 0;
    else
    {
        for (int32_t i = -1; i <= 1; ++i)
        {
            for (int32_t j = -1; j <= 1; ++j)
            {
                int32_t pi_w = j + w;
                int32_t pi_h = i + h;

                int32_t px = imgs[0]->get(pi_w, pi_h);

                int32_t r = px >> 24;
                int32_t g = px >> 16;
                int32_t b = px >> 8;

                int32_t nc = (r + g + b) / 3;

                sum_x = sum_x + nc * gx[j + 1][i + 1];
                sum_y = sum_y + nc * gy[j + 1][i + 1];
            }
        }
    }

    sum = fabs(sum_x) + fabs(sum_y);

    if (sum > 255)
        sum = 255;
    else if (sum < 0)
        sum = 0;

    uint8_t new_px = (uint8_t)sum;

    imgs[1]->set(w, h, rgba(new_px, new_px, new_px, -1));
}
