#include "binarize.hh"

#include <iostream>
#include <misc/color.hh>

using namespace filters;

void Binarize::init(VectStr_ptr argv)
{
    if (argv->size() <= 1)
    {
        std::cerr << "Not enough arguments for " << (*argv)[0] << std::endl;
        exit(1);
    }

    limit_ = atoi((*argv)[1].c_str());
}

void Binarize::apply(std::vector<Image_ptr>& imgs, uint16_t w, uint16_t h)
{
    imgs[0]->set(w, h, imgs[0]->get(w, h) >= limit_
            ? rgba(255, 255, 255, 255) : rgba(0, 0, 0, 255));
}
