#include "blur.hh"

#include <cmath>
#include <iostream>
#include <tbb/parallel_for.h>
#include <tbb/blocked_range2d.h>

#include <misc/color.hh>

using namespace filters;

void Blur::init(VectStr_ptr argv)
{
    if (argv->size() <= 1)
    {
        std::cerr << "Not enough arguments for " << (*argv)[0] << std::endl;
        exit(1);
    }

    radius_ = atoi((*argv)[1].c_str());
}

void Blur::run(std::vector<Image_ptr>& imgs)
{
    Image_ptr new_img(new Image(opts_.width(), opts_.height()));
    imgs.push_back(new_img);

    parallel_for(
        tbb::blocked_range2d<size_t>(0, opts_.width(), 0, opts_.height()),
        [&](const tbb::blocked_range2d<size_t>& r)
        {
            for (size_t w = r.rows().begin(); w != r.rows().end(); ++w)
                for (size_t h = r.cols().begin(); h != r.cols().end(); ++h)
                    apply(imgs, w, h);
        }
    );

    img_ = imgs[1];
}

void Blur::apply(std::vector<Image_ptr>& imgs, uint16_t w, uint16_t h)
{
    uint32_t r = 0;
    uint32_t g = 0;
    uint32_t b = 0;
    uint32_t a = 0;
    uint32_t div = 0;

    for (int32_t w_i = w - radius_; w_i <= w + radius_; ++w_i)
    {
        if (w_i < 0 || w_i >= opts_.width())
            continue;

        for (int32_t h_j = h - radius_; h_j <= h + radius_; ++h_j)
        {
            if (h_j < 0 || h_j >= opts_.height())
                continue;

            ++div;

            r += ((imgs[0]->get(w_i, h_j) >> 24) & 0xFF);
            g += ((imgs[0]->get(w_i, h_j) >> 16) & 0xFF);
            b += ((imgs[0]->get(w_i, h_j) >> 8) & 0xFF);
            a += (imgs[0]->get(w_i, h_j) & 0xFF);
        }
    }

    imgs[1]->set(w, h, rgba(r / div, g / div, b / div, a / div));
}
