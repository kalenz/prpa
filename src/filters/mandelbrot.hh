#ifndef FILTER_MANDELBROT_HH
# define FILTER_MANDELBROT_HH

# include <vector>
# include <misc/types.hh>
# include "set.hh"

namespace filters
{

class Mandelbrot : public Set
{
    DISALLOW_COPY_AND_ASSIGN(Mandelbrot)

public:
    Mandelbrot(const Options& opts);
    virtual void apply(std::vector<Image_ptr>& imgs, uint16_t w, uint16_t h);
};

inline
Mandelbrot::Mandelbrot(const Options& opts)
    : Set(opts)
{
    nb_arg_ = 0;
}

} // namespace filters

#endif // FILTER_MANDELBROT_HH
