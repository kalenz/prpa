#ifndef FILTERS_BINARIZE_HH
# define FILTERS_BINARIZE_HH

# include <cstdint>
# include <vector>
# include <string>
# include <memory>

# include <misc/types.hh>
# include <misc/image.hh>
# include <misc/options.hh>

# include "filter.hh"

namespace filters
{

class Binarize : public Filter
{
    DISALLOW_COPY_AND_ASSIGN(Binarize)

public:
    Binarize(const Options& opts);
    virtual void init(VectStr_ptr argv);
    virtual void apply(std::vector<Image_ptr>& imgs,
            uint16_t w, uint16_t h);

private:
    uint32_t limit_;
};

inline
Binarize::Binarize(const Options& opts)
    : Filter(opts)
{
    nb_arg_ = 1;
}

} // namespace filters

#endif // FILTERS_BINARIZE_HH
