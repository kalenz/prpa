#ifndef FILTERS_COLORIZE_HH
# define FILTERS_COLORIZE_HH

# include <cstdint>
# include <vector>
# include <string>
# include <memory>

# include <misc/types.hh>
# include <misc/image.hh>
# include <misc/color.hh>
# include <misc/options.hh>

# include "filter.hh"

namespace filters
{

class Colorize : public Filter
{
    DISALLOW_COPY_AND_ASSIGN(Colorize)

public:
    Colorize(const Options& opts);
    virtual void init(VectStr_ptr argv);
    virtual void apply(std::vector<Image_ptr>& imgs,
            uint16_t w, uint16_t h);

private:
    struct rgba rgba_;
};

inline
Colorize::Colorize(const Options& opts)
    : Filter(opts)
{
    nb_arg_ = 1;
}

} // namespace filters

#endif // FILTERS_COLORIZE_HH
