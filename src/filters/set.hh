#ifndef FILTERS_SET_HH
# define FILTERS_SET_HH

# include <misc/types.hh>
# include "sets.hh"
# include "filter.hh"

namespace filters
{

class Set : public Filter
{
public:
    Set(const Options& opts);
    virtual void init(VectStr_ptr argv);
    void run(std::vector<Image_ptr>& imgs);
    virtual void apply(std::vector<Image_ptr>& imgs, uint16_t w, uint16_t h) = 0;

protected:
    struct set_options sopts_;
};

} // namespace filters

#endif // FILTERS_SET_HH
