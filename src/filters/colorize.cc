#include "colorize.hh"

#include <iostream>
#include <misc/color.hh>

using namespace filters;

void Colorize::init(VectStr_ptr argv)
{
    if (argv->size() <= 4)
    {
        std::cerr << "Not enough arguments for " << (*argv)[0] << std::endl;
        exit(1);
    }

    rgba_.r = atoi((*argv)[1].c_str());
    rgba_.g = atoi((*argv)[2].c_str());
    rgba_.b = atoi((*argv)[3].c_str());
    rgba_.a = atoi((*argv)[4].c_str());
}

void Colorize::apply(std::vector<Image_ptr>& imgs, uint16_t w, uint16_t h)
{
    uint32_t coeff = imgs[0]->get(w, h);
    imgs[0]->set(w, h, rgba(coeff | rgba_.r, coeff | rgba_.g,
                coeff | rgba_.b, rgba_.a));
}
