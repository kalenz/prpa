#include "and.hh"

using namespace filters;

void And::apply(std::vector<Image_ptr>& imgs, uint16_t w, uint16_t h)
{
    imgs[0]->set(w, h, (imgs[0]->get(w, h) & imgs[1]->get(w, h)) | 0xFF);
}
