#ifndef FILTERS_XOR_HH
# define FILTERS_XOR_HH

# include <cstdint>
# include <vector>

# include <misc/types.hh>
# include <misc/image.hh>
# include <misc/options.hh>

# include "filter.hh"

namespace filters
{

class Xor : public Filter
{
    DISALLOW_COPY_AND_ASSIGN(Xor)

public:
    Xor(const Options& opts);
    void run(std::vector<Image_ptr>& imgs);
    virtual void apply(std::vector<Image_ptr>& imgs,
            uint16_t w, uint16_t h);
};

inline
Xor::Xor(const Options& opts)
    : Filter(opts)
{
    nb_arg_ = 2;
}

} // namespace filters

#endif // FILTERS_XOR_HH
