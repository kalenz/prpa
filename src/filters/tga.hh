#ifndef FILTERS_TGA_HH
# define FILTERS_TGA_HH

# include <vector>
# include <cstdint>
# include <string>

# include <misc/types.hh>
# include <misc/image.hh>
# include <misc/options.hh>
# include "filter.hh"

namespace filters
{

class TGA : public Filter
{
    DISALLOW_COPY_AND_ASSIGN(TGA)

public:
    TGA(const Options& opts);
    virtual void init(VectStr_ptr argv);
    virtual void run(std::vector<Image_ptr>& imgs);
    virtual void apply(std::vector<Image_ptr>& imgs, uint16_t w, uint16_t h);

private:
    std::string filename_;
};

inline
TGA::TGA(const Options& opts)
    : Filter(opts)
{
    nb_arg_ = 1;
}

} // namespace filters

#endif // FILTERS_TGA_HH
