#ifndef FILTERS_SOBEL_HH
# define FILTERS_SOBEL_HH

# include <cstdint>
# include <vector>
# include <string>
# include <memory>

# include <misc/types.hh>
# include <misc/image.hh>
# include <misc/options.hh>

# include "filter.hh"

namespace filters
{

class Sobel : public Filter
{
    DISALLOW_COPY_AND_ASSIGN(Sobel)

public:
    Sobel(const Options& opts);
    virtual void run(std::vector<Image_ptr>& imgs);
    virtual void apply(std::vector<Image_ptr>& imgs,
            uint16_t w, uint16_t h);
};

inline
Sobel::Sobel(const Options& opts)
    : Filter(opts)
{
    nb_arg_ = 1;
}

} // namespace filters

#endif // FILTERS_SOBEL_HH
