#include "xor.hh"

using namespace filters;

void Xor::run(std::vector<Image_ptr>& imgs)
{
    for (size_t w = 0; w < opts_.width(); ++w)
        for (size_t h = 0; h < opts_.height(); ++h)
            apply(imgs, w, h);
    img_ = imgs[0];
}

void Xor::apply(std::vector<Image_ptr>& imgs, uint16_t w, uint16_t h)
{
    imgs[0]->set(w, h, (imgs[0]->get(w, h) ^ imgs[1]->get(w, h)) | 0xFF);
}
