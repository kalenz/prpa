#ifndef FILTERS_BLUR_HH
# define FILTERS_BLUR_HH

# include <cstdint>
# include <vector>
# include <string>
# include <memory>

# include <misc/types.hh>
# include <misc/image.hh>
# include <misc/options.hh>

# include "filter.hh"

namespace filters
{

class Blur : public Filter
{
    DISALLOW_COPY_AND_ASSIGN(Blur)

public:
    Blur(const Options& opts);
    virtual void init(VectStr_ptr argv);
    virtual void run(std::vector<Image_ptr>& imgs);
    virtual void apply(std::vector<Image_ptr>& imgs,
            uint16_t w, uint16_t h);

private:
    uint16_t radius_;
};

inline
Blur::Blur(const Options& opts)
    : Filter(opts)
{
    nb_arg_ = 1;
}

} // namespace filters

#endif // FILTERS_BLUR_HH
