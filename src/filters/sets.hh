#ifndef SETS_HH
# define SETS_HH

# include <cstdint>

# include <misc/options.hh>

namespace filters
{

struct set_options
{
    uint32_t max_iter;
    double center_x;
    double center_y;
    double min_re;
    double max_re;
    double min_im;
    double max_im;
};

namespace utils
{
    inline
    double get_origin_x(uint16_t xi, struct set_options& sopts,
            const Options& opts)
    {
        return (sopts.min_re + xi * (sopts.max_re - sopts.min_re) /
                (opts.width() - 1)) + sopts.center_x;
    }

    inline
    double get_origin_y(uint16_t yi, struct set_options& sopts,
            const Options& opts)
    {
        return (sopts.max_im - yi * (sopts.max_im - sopts.min_im) /
                (opts.height() - 1)) + sopts.center_y;
    }
}

} // namespace filters

#endif // SETS_HH
