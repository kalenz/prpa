#include "burningship.hh"

#include <cstdint>
#include <vector>
#include <string>
#include <cmath>
#include "sets.hh"

#define BAIL2   (2 * 2)

using namespace filters;

void Burningship::apply(std::vector<Image_ptr>& imgs, uint16_t w, uint16_t h)
{
    double x = 0;
    double y = 0;
    double xtemp;
    uint32_t iter;

    double x0 = utils::get_origin_x(w, sopts_, opts_);
    double y0 = utils::get_origin_y(h, sopts_, opts_);

    for (iter = 0; (x * x + y * y <= BAIL2) && (iter < sopts_.max_iter); ++iter)
    {
        xtemp = x * x - y * y - x0;
        y = 2 * fabs(x * y) - y0;
        x = xtemp;
    }

    imgs[0]->set(w, h, iter);
}
