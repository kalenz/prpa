#include "filter.hh"

#include <vector>
#include <tbb/parallel_for.h>
#include <tbb/blocked_range2d.h>
#include <misc/image.hh>

using namespace filters;

void Filter::run(std::vector<Image_ptr>& imgs)
{
    parallel_for(
        tbb::blocked_range2d<size_t>(0, opts_.width(), 0, opts_.height()),
        [&](const tbb::blocked_range2d<size_t>& r)
        {
            for (size_t w = r.rows().begin(); w != r.rows().end(); ++w)
                for (size_t h = r.cols().begin(); h != r.cols().end(); ++h)
                    apply(imgs, w, h);
        }
    );

    img_ = imgs[0];
}
