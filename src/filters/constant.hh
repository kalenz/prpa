#ifndef FILTER_CONSTANT_HH
# define FILTER_CONSTANT_HH

# include <vector>
# include <misc/types.hh>
# include <misc/color.hh>
# include "set.hh"

namespace filters
{

class Constant : public Set
{
    DISALLOW_COPY_AND_ASSIGN(Constant)

public:
    Constant(const Options& opts);
    virtual void init(VectStr_ptr argv);
    virtual void apply(std::vector<Image_ptr>& imgs, uint16_t w, uint16_t h);

private:
    struct rgba rgba_;
};

} // namespace filters

#endif // FILTER_CONSTANT_HH
