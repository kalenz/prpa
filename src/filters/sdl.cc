#include "sdl.hh"

#include <vector>
#include <cstdint>

#include <misc/image.hh>

#include <display/sdl.hh>

using namespace filters;

void SDL::run(std::vector<Image_ptr>& imgs)
{
    display::SDL disp;
    disp.init(imgs[0]);
    disp.display();
    disp.quit();

    img_ = imgs[0];
}

void SDL::apply(std::vector<Image_ptr>& imgs, uint16_t w, uint16_t h)
{
    (void) imgs;
    (void) w;
    (void) h;
}
