#ifndef FILTERS_SDL_HH
# define FILTERS_SDL_HH

# include <vector>
# include <cstdint>

# include <misc/types.hh>
# include <misc/image.hh>
# include <misc/options.hh>
# include "filter.hh"

namespace filters
{

class SDL : public Filter
{
    DISALLOW_COPY_AND_ASSIGN(SDL)

public:
    SDL(const Options& opts);
    virtual void run(std::vector<Image_ptr>& imgs);
    virtual void apply(std::vector<Image_ptr>& imgs, uint16_t w, uint16_t h);
};

inline
SDL::SDL(const Options& opts)
    : Filter(opts)
{
    nb_arg_ = 1;
}

} // namespace filters

#endif // FILTERS_SDL_HH
