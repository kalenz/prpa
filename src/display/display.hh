#ifndef DISPLAY_HH
# define DISPLAY_HH

# include <memory>
# include <misc/types.hh>
# include <misc/image.hh>

namespace display
{

/* abstract */
class Display
{
    DISALLOW_COPY_AND_ASSIGN(Display)

public:
    Display() {}
    virtual void init(Image_ptr img, std::string filename = "out") = 0;
    virtual void display() = 0;
    virtual void quit() = 0;
protected:
    Image_ptr img_;
};

typedef std::shared_ptr<Display> Display_ptr;

} // namespace display

#endif // DISPLAY_HH
