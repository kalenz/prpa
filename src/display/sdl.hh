#ifndef SDL_HH
# define SDL_HH

# include <cstdint>
# include <string>
# include <iostream>
# include <misc/types.hh>
# include <SDL.h>

# include "display.hh"

namespace display
{

class SDL : public Display
{
    DISALLOW_COPY_AND_ASSIGN(SDL)

public:
    SDL() : Display() {}
    virtual void init(Image_ptr img, std::string f);
    virtual void display();
    virtual void quit();

private:
    SDL_Surface* surface_;
};

inline
void SDL::init(Image_ptr img, std::string f = "nouse")
{
    (void) f;
    img_ = img;

    if (SDL_Init(SDL_INIT_VIDEO) != 0)
        std::cerr << "Could not init SDL" << std::endl;

    if ((surface_ = SDL_SetVideoMode(img_->width(),
                    img_->height(), 32, SDL_DOUBLEBUF)) == NULL)
        std::cerr << "Could not set video mode" << std::endl;

    SDL_WM_SetCaption("Kfract", NULL);
}

inline
void SDL::display()
{
    SDL_LockSurface(surface_);
    Uint32 px;
    for (uint16_t w = 0; w < img_->width(); ++w)
    {
        for (uint16_t h = 0; h < img_->height(); ++h)
        {
            px = img_->get(w, h);
            ((Uint32*)surface_->pixels)[h * img_->width() + w] =
                SDL_MapRGBA(surface_->format, px >> 24, px >> 16,
                        px >> 8, px);
        }
    }
    SDL_UnlockSurface(surface_);

    SDL_UpdateRect(surface_, 0, 0, 0, 0);
}

} // namespace display

#endif // SDL_HH
