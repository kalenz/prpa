#ifndef TGA_HH
# define TGA_HH

# include <fstream>
# include <iostream>
# include <misc/types.hh>
# include <misc/image.hh>

# include "display.hh"

namespace display
{

class TGA : public Display
{
    DISALLOW_COPY_AND_ASSIGN(TGA)

public:
    TGA() : Display() {}
    virtual void init(Image_ptr img, std::string filename);
    virtual void display();
    virtual void quit();

private:
    std::ofstream os_;
};

inline
void TGA::init(Image_ptr img, std::string filename = "out.tga")
{
    img_ = img;

    os_.open(filename);

    if (!os_.is_open())
    {
        std::cerr << "Could not open output file" << std::endl;
        exit(1);
    }
}

inline
void TGA::quit()
{
    os_.close();
}

} // namespace display

#endif // DISPLAY_HH
