#include "sdl.hh"

using namespace display;

void SDL::quit()
{
    SDL_Event event;

    for (bool quit = false; !quit; quit = (event.type == SDL_QUIT))
        SDL_WaitEvent(&event);

    SDL_Quit();
}
