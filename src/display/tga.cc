/*
** This code was originally taken from PCTOS2
** http://code.delroth.net/pctos2
*/

#include "tga.hh"

#include <cstdint>
#include <fstream>
#include <boost/detail/endian.hpp>

#include <misc/image.hh>

# ifdef BOOST_LITTLE_ENDIAN
#  define DECLARE_FOR_SIZE(n) \
    inline uint##n##_t from_le##n(uint##n##_t num) \
    { \
        return num; \
    }
# else
#  define DECLARE_FOR_SIZE(n) \
    inline uint##n##_t from_le##n(uint##n##_t num) \
    { \
        return swap##(num); \
    }
# endif

DECLARE_FOR_SIZE(16)

static void write_tga(std::ofstream& of, const Image_ptr img)
{
    of.write("\x00\x00\x02\x00\x00\x00\x00\x00\x00\x00\x00\x00", 12);

    uint16_t w_le = from_le16(img->width());
    uint16_t h_le = from_le16(img->height());

    of.write((char*)&w_le, 2);
    of.write((char*)&h_le, 2);

    of.write("\x20\x00", 2);

    const uint32_t* data = img->data();
    for (uint16_t y = img->height(); y > 0; --y)
    {
        for (uint16_t x = 0; x < img->width(); ++x)
        {
            uint32_t pix = data[(y - 1) * img->width() + x];

            uint8_t r = (pix & 0xFF000000) >> 24;
            uint8_t g = (pix & 0x00FF0000) >> 16;
            uint8_t b = (pix & 0x0000FF00) >> 8;
            uint8_t a = (pix & 0x000000FF) >> 0;

            of.write((char*)&b, 1);
            of.write((char*)&g, 1);
            of.write((char*)&r, 1);
            of.write((char*)&a, 1);
        }
    }
}

using namespace display;

void TGA::display()
{
    write_tga(os_, img_);
}
