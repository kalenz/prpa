# Makefile for PRPA project
# Nicolas Hureau

CXXFLAGS	= -std=c++0x -Wall -Wextra -pedantic -g -I./src \
		  $(shell pkg-config --cflags sdl)
LDFLAGS		= -lboost_program_options -ltbb_debug -lrt -lm \
		  $(shell pkg-config --libs sdl)

PROJNAME	= prpa

SRC		= src/$(PROJNAME).cc \
		  src/misc/options.cc \
		  src/misc/image.cc \
		  src/misc/parser.cc \
		  src/display/tga.cc \
		  src/display/sdl.cc \
		  src/filters/filter.cc \
		  src/filters/set.cc \
		  src/filters/mandelbrot.cc \
		  src/filters/burningship.cc \
		  src/filters/constant.cc \
		  src/filters/tga.cc \
		  src/filters/sdl.cc \
		  src/filters/binarize.cc \
		  src/filters/colorize.cc \
		  src/filters/unify.cc \
		  src/filters/sobel.cc \
		  src/filters/blur.cc \
		  src/filters/xor.cc \
		  src/filters/and.cc \
		  src/filters/or.cc

OBJ		= $(SRC:.cc=.o)
DEP		= $(SRC:.cc=.d)

.PHONY: all clean distclean

all: $(PROJNAME)

-include $(DEP)
$(PROJNAME): $(OBJ)
	$(CXX) $(LDFLAGS) -o $@ $(OBJ)

clean:
	rm -f $(OBJ)

distclean: clean
	rm -f $(DEP)
	rm -f $(PROJNAME)

# EOF
